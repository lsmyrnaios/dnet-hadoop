<workflow-app name="affiliation_from_semrel_propagation" xmlns="uri:oozie:workflow:0.5">
    <parameters>
        <property>
            <name>sourcePath</name>
            <description>the source path</description>
        </property>
        <property>
            <name>outputPath</name>
            <description>sets the outputPath</description>
        </property>
    </parameters>

    <global>
        <job-tracker>${jobTracker}</job-tracker>
        <name-node>${nameNode}</name-node>
        <configuration>
            <property>
                <name>oozie.action.sharelib.for.spark</name>
                <value>${oozieActionShareLibForSpark2}</value>
            </property>
        </configuration>
    </global>

    <start to="resume_from"/>

    <kill name="Kill">
        <message>Action failed, error message[${wf:errorMessage(wf:lastErrorNode())}]</message>
    </kill>

    <decision name="resume_from">
        <switch>
            <case to="prepare_info">${wf:conf('resumeFrom') eq 'PrepareInfo'}</case>
            <default to="reset_outputpath"/> <!-- first action to be done when downloadDump is to be performed -->
        </switch>
    </decision>

    <action name="reset_outputpath">
        <fs>
            <delete path="${outputPath}"/>
            <mkdir path="${outputPath}"/>
        </fs>
        <ok to="copy_entities"/>
        <error to="Kill"/>
    </action>

    <fork name="copy_entities">
        <path start="copy_relation"/>
        <path start="copy_publication"/>
        <path start="copy_dataset"/>
        <path start="copy_orp"/>
        <path start="copy_software"/>
        <path start="copy_organization"/>
        <path start="copy_projects"/>
        <path start="copy_datasources"/>
    </fork>

    <action name="copy_relation">
        <distcp xmlns="uri:oozie:distcp-action:0.2">
            <arg>${nameNode}/${sourcePath}/relation</arg>
            <arg>${nameNode}/${outputPath}/relation</arg>
        </distcp>
        <ok to="wait"/>
        <error to="Kill"/>
    </action>

    <action name="copy_publication">
        <distcp xmlns="uri:oozie:distcp-action:0.2">
            <arg>${nameNode}/${sourcePath}/publication</arg>
            <arg>${nameNode}/${outputPath}/publication</arg>
        </distcp>
        <ok to="wait"/>
        <error to="Kill"/>
    </action>

    <action name="copy_dataset">
        <distcp xmlns="uri:oozie:distcp-action:0.2">
            <arg>${nameNode}/${sourcePath}/dataset</arg>
            <arg>${nameNode}/${outputPath}/dataset</arg>
        </distcp>
        <ok to="wait"/>
        <error to="Kill"/>
    </action>

    <action name="copy_orp">
        <distcp xmlns="uri:oozie:distcp-action:0.2">
            <arg>${nameNode}/${sourcePath}/otherresearchproduct</arg>
            <arg>${nameNode}/${outputPath}/otherresearchproduct</arg>
        </distcp>
        <ok to="wait"/>
        <error to="Kill"/>
    </action>

    <action name="copy_software">
        <distcp xmlns="uri:oozie:distcp-action:0.2">
            <arg>${nameNode}/${sourcePath}/software</arg>
            <arg>${nameNode}/${outputPath}/software</arg>
        </distcp>
        <ok to="wait"/>
        <error to="Kill"/>
    </action>

    <action name="copy_organization">
        <distcp xmlns="uri:oozie:distcp-action:0.2">
            <arg>${nameNode}/${sourcePath}/organization</arg>
            <arg>${nameNode}/${outputPath}/organization</arg>
        </distcp>
        <ok to="wait"/>
        <error to="Kill"/>
    </action>

    <action name="copy_projects">
        <distcp xmlns="uri:oozie:distcp-action:0.2">
            <arg>${nameNode}/${sourcePath}/project</arg>
            <arg>${nameNode}/${outputPath}/project</arg>
        </distcp>
        <ok to="wait"/>
        <error to="Kill"/>
    </action>

    <action name="copy_datasources">
        <distcp xmlns="uri:oozie:distcp-action:0.2">
            <arg>${nameNode}/${sourcePath}/datasource</arg>
            <arg>${nameNode}/${outputPath}/datasource</arg>
        </distcp>
        <ok to="wait"/>
        <error to="Kill"/>
    </action>

    <join name="wait" to="prepare_info"/>


    <action name="prepare_info">
        <spark xmlns="uri:oozie:spark-action:0.2">
            <master>yarn</master>
            <mode>cluster</mode>
            <name>PrepareResultOrganizationAssociation</name>
            <class>eu.dnetlib.dhp.entitytoorganizationfromsemrel.PrepareInfo</class>
            <jar>dhp-enrichment-${projectVersion}.jar</jar>
            <spark-opts>
                --executor-cores=${sparkExecutorCores}
                --executor-memory=${sparkExecutorMemory}
                --driver-memory=${sparkDriverMemory}
                --conf spark.extraListeners=${spark2ExtraListeners}
                --conf spark.sql.queryExecutionListeners=${spark2SqlQueryExecutionListeners}
                --conf spark.yarn.historyServer.address=${spark2YarnHistoryServerAddress}
                --conf spark.eventLog.dir=${nameNode}${spark2EventLogDir}
            </spark-opts>
            <arg>--graphPath</arg><arg>${sourcePath}</arg>
            <arg>--hive_metastore_uris</arg><arg>${hive_metastore_uris}</arg>
            <arg>--leavesPath</arg><arg>${workingDir}/preparedInfo/leavesPath</arg>
            <arg>--childParentPath</arg><arg>${workingDir}/preparedInfo/childParentPath</arg>
            <arg>--resultOrgPath</arg><arg>${workingDir}/preparedInfo/resultOrgPath</arg>
            <arg>--projectOrganizationPath</arg><arg>${workingDir}/preparedInfo/projectOrganizationPath</arg>
            <arg>--relationPath</arg><arg>${workingDir}/preparedInfo/relation</arg>
        </spark>
        <ok to="apply_resulttoorganization_propagation"/>
        <error to="Kill"/>
    </action>

    <action name="apply_resulttoorganization_propagation">
        <spark xmlns="uri:oozie:spark-action:0.2">
            <master>yarn</master>
            <mode>cluster</mode>
            <name>resultToOrganizationFromSemRel</name>
            <class>eu.dnetlib.dhp.entitytoorganizationfromsemrel.SparkResultToOrganizationFromSemRel</class>
            <jar>dhp-enrichment-${projectVersion}.jar</jar>
            <spark-opts>
                --executor-cores=${sparkExecutorCores}
                --executor-memory=${sparkExecutorMemory}
                --driver-memory=${sparkDriverMemory}
                --conf spark.extraListeners=${spark2ExtraListeners}
                --conf spark.sql.queryExecutionListeners=${spark2SqlQueryExecutionListeners}
                --conf spark.yarn.historyServer.address=${spark2YarnHistoryServerAddress}
                --conf spark.eventLog.dir=${nameNode}${spark2EventLogDir}
                --conf spark.dynamicAllocation.enabled=true
                --conf spark.dynamicAllocation.maxExecutors=${spark2MaxExecutors}
                --conf spark.sql.shuffle.partitions=3840
            </spark-opts>
            <arg>--relationPath</arg><arg>${workingDir}/preparedInfo/relation</arg>
            <arg>--outputPath</arg><arg>${outputPath}/relation</arg>
            <arg>--leavesPath</arg><arg>${workingDir}/preparedInfo/leavesPath</arg>
            <arg>--childParentPath</arg><arg>${workingDir}/preparedInfo/childParentPath</arg>
            <arg>--resultOrgPath</arg><arg>${workingDir}/preparedInfo/resultOrgPath</arg>
            <arg>--projectOrganizationPath</arg><arg>${workingDir}/preparedInfo/projectOrganizationPath</arg>
            <arg>--hive_metastore_uris</arg><arg>${hive_metastore_uris}</arg>
            <arg>--workingDir</arg><arg>${workingDir}/working</arg>
            <arg>--iterations</arg><arg>${iterations}</arg>
        </spark>
        <ok to="End"/>
        <error to="Kill"/>
    </action>



    <end name="End"/>

</workflow-app>